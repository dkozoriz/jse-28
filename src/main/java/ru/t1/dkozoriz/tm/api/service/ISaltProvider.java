package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

}