package ru.t1.dkozoriz.tm.exception.field;

public final class EmailIsExistException extends AbstractFieldException {

    public EmailIsExistException() {
        super("Error! Email is exist.");
    }

}