package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    public UserLoginCommand() {
        super("login", "user login.");
    }

    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}